(use-modules  (guix packages)
	      (gnu packages)
	      (gnu packages base)
	      (srfi srfi-1))


(define specs '("dash"
		"coreutils-minimal"
		"less"
		"lesspipe"
		"tar"
		"gzip"
		"sed"
		"gawk"))



(specifications->manifest `(,@specs))
